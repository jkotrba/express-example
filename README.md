<img src="http://i.imgur.com/UzC7XPe.png" alt="Helio Training" width="226" align="center"/> v1.0.0

---------------

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com)

# Boilerplate

Basic example exercise for the new platform. Make sure you're using the new application when running the below commands.

## Instructions

```sh
# Install dependencies
yarn install

# Start the application
yarn start

# Build the application
yarn build

# Test the application
yarn test
```
